﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems;
using TMPro;
using UnityEngine.SceneManagement;

//Author: Simon Weickert

public class ObjectSpawner : MonoBehaviour
{
    private ARPlaneManager arPlaneManager;
    private ARRaycastManager arRaycastManager;
    private EventPublisher eventPublisher;

    List<ARRaycastHit> hits = new List<ARRaycastHit>();

    private GameObject verticalPlane;
    private Camera arCamera;
    private RaycastHit hitObject;
    private bool allowedToPlacePicture = false;
    private bool firstVerticalARPlacement = true;
    private GameObject objectToSpawn; //Daniel Stöcklein -- auf private gesetzt
    private GameObject placedObject;

    [SerializeField]
    private GameObject measureTextPrefab;
    private GameObject widthMeasure;
    private GameObject heightMeasure;

    private UIController uiController;
    private GameObject sceneManager;
    private SceneLoader sceneLoader;
    [SerializeField]
    private GameObject sculptureModeButton;
    private bool uiTouched;


    private ARPlane arPlane;
    private GameObject child; //Daniel Stöcklein


    private Selectable selectable; //Daniel Stöcklein
    Ray ray;

    private void Action_OnTouchAndRaycastHitPhysicsObject(RaycastHit hitObject)
    {
        if (allowedToPlacePicture == true && hitObject.transform.gameObject.name.Contains("VerticalPlane"))
        {
            placePicture(hitObject.point, hitObject.transform.rotation);

            if (placedObject.transform.parent == null)
                placedObject.transform.SetParent(hitObject.transform, true);
            selectable.Select(placedObject); //Daniel Stöcklein
            allowedToPlacePicture = false;
        }
    }


    public GameObject getPlacedObject()
    {
        return placedObject;
    }

    public void objectSpawnerState(bool allowed)
    {
        allowedToPlacePicture = allowed;
    }


    public void setMeasureTextOfPlacedObject(GameObject placedObject)
    {
        GameObject measures;
        //Daniel Stöcklein - Besser man sucht nach Tag, damit man child objekte zum rahmen hinzufügen kann ohne den code zu ändern
        measures = TagSearcher.FindObjectsWithTag(placedObject.transform, "Hoehe");
        if (measures != null)
        {
            TextMeshPro height_TMP = measures.GetComponent<TextMeshPro>();
            height_TMP.SetText("Höhe: " + Mathf.Round(placedObject.GetComponent<MeshFilter>().mesh.bounds.size.y * placedObject.transform.lossyScale.y * 100) + " cm");
        }
        else
        {
            Debug.Log("Please add a height object to the frame");
        }

        measures = TagSearcher.FindObjectsWithTag(placedObject.transform, "Breite");
        if (measures != null)
        {
            TextMeshPro width_TMP = measures.GetComponent<TextMeshPro>();
            width_TMP.SetText("Breite: " + Mathf.Round(placedObject.GetComponent<MeshFilter>().mesh.bounds.size.x * placedObject.transform.lossyScale.x * 100) + " cm");
        }
        else
        {
            Debug.Log("Please add a Width object to the frame");
        }
    }

    public void placePicture(Vector3 pos, Quaternion rot)
    {
        if (objectToSpawn != null)
        {
            child.transform.GetComponent<Renderer>().material.name = "m2"; //Daniel Stöcklein (siehe PP4S20AM-84) Kein guter Fix, aber funktioniert

            placedObject = Instantiate(objectToSpawn, pos, rot);
            placedObject.name = objectToSpawn.name;

            setMeasureTextOfPlacedObject(placedObject);
            Debug.Log(placedObject.name + " instantiated");
        }
        else
        {
            Debug.Log("Picture cannot be placed because picture is not yet specified");
        }
    }

    public void changeTransparency(float alpha)
    {
        if (placedObject != null)
        {
            Material materialFrame = placedObject.GetComponent<MeshRenderer>().material;
            materialFrame.color = new Color(materialFrame.color.r, materialFrame.color.g, materialFrame.color.b, alpha);

            Material materialPicture = TagSearcher.FindObjectsWithTag(placedObject.transform, "Picture").GetComponent<Renderer>().material;
            materialPicture.color = new Color(materialPicture.color.r, materialPicture.color.g, materialPicture.color.b, alpha);
        }
    }

     //Daniel Stöcklein -- Läd Rahmen prefab und platziert Bild
    private void SetUpPrefab()
    {
        if (PersistentManager.Instance.frameName == null || PersistentManager.Instance.picture == null)
        {
            Debug.Log("PersistentManager: Something went wrong");
            return;
        }

        objectToSpawn = Resources.Load("Prefabs/" + PersistentManager.Instance.frameName) as GameObject; //lade Rahmen

        child = TagSearcher.FindObjectsWithTag(objectToSpawn.transform, "Picture");
        if (child != null)
        {
            child.gameObject.GetComponent<Renderer>().material = PersistentManager.Instance.picture; //platziere bild
        }
        else
        {
            Debug.Log("Please add a picture to the frame");
        }
    }
    void Awake()
    {
        arCamera = Camera.main;
        selectable = new Selectable(); //Daniel Stöcklein
        uiController = new UIController();
        sceneManager = GameObject.FindGameObjectWithTag("Manager");
        sceneLoader = sceneManager.GetComponent<SceneLoader>();

        arPlaneManager = GetComponent<ARPlaneManager>();
        arRaycastManager = FindObjectOfType<ARRaycastManager>();
        eventPublisher = FindObjectOfType<EventPublisher>();


        eventPublisher.OnTouchAndRaycastHitPhysicsObject += Action_OnTouchAndRaycastHitPhysicsObject;

    }

    void OnEnable()
    {
        SetUpPrefab();

        if (SceneManager.GetSceneByName("ARSculpture").isLoaded)
        {
            sculptureModeButton.SetActive(sceneLoader.IsExperimentalModeActive());
        }
    }
    void OnDestroy()
    {
        eventPublisher.OnTouchAndRaycastHitPhysicsObject -= Action_OnTouchAndRaycastHitPhysicsObject;
    }
}
