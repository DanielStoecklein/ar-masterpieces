﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.Networking;

namespace UnsplashSearch
{
    public class RequestHandler : MonoBehaviour
    {
        private void Start()
        {
            RequestHandler rh = gameObject.AddComponent<RequestHandler>();
            StartCoroutine(rh.SearchPhotos("plane"));
        }

        private const string secretkey = "kTC4adtBtenbjWlRfR-_s6OTR7xPBDKg7ZpMuO2FWaE";
        private const string acceskey = "lw_UQYcYNX9uJQB3qpzrdF7-QxqW8NaqZzFhkBNpN9o";
        public readonly IDictionary<string, string> UriParts = new Dictionary<string, string>() {
            { "base", "https://api.unsplash.com/" },
            { "photos", "photos" },
            { "search", "search/photos" }
        };


        public IEnumerator SearchPhotos(string suchbegriff)
        {

            string searchurl = UriParts["base"] + UriParts["search"] + "?query=" + suchbegriff;

            UnityWebRequest searchrequest = UnityWebRequest.Get(searchurl);
            searchrequest.SetRequestHeader("Authorization", "Client-ID " + acceskey);

            yield return searchrequest.SendWebRequest();

            if (searchrequest.isNetworkError || searchrequest.isHttpError)
            {
                Debug.Log(searchrequest.error);
            }
            else
            {
                StreamWriter writer = new StreamWriter(Path.Combine(new string[] { "Assets", "Scripts", "Search", "searchresult.json" }));
                string answerbody = searchrequest.downloadHandler.text;
                writer.Write(answerbody);
                writer.Close();

                
                //SearchResults results = JsonUtility.FromJson<SearchResults>(answerbody);
            }

        }
    }
}
