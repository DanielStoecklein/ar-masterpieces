using UnityEngine;

//Author: Simon Weickert

public class Logger : MonoBehaviour
{
    //#if !UNITY_EDITOR
    static string myLog = "";
    private string output;
    private string stack;

    private GUIStyle textStyle = new GUIStyle();

    void Start()
    {
        Application.logMessageReceived += Log;
        DontDestroyOnLoad(this);
    }

    void OnDestroy()
    {
        Application.logMessageReceived -= Log;
    }

    public void Log(string logString, string stackTrace, LogType type)
    {
        output = logString;
        stack = stackTrace;
        if (type != LogType.Warning)
        {
            if (stack.Length > 0)
            {
                output = output + "\n -> Stracktrace: " + stack;
            }
            myLog = output + "\n" + myLog;
        }
        if (myLog.Length > 5000)
        {

            myLog = myLog.Substring(0, 4000);
        }
    }

    void OnGUI()
    {
        textStyle.fontSize = 20;
        textStyle.normal.textColor = Color.cyan;
        GUI.Label(new Rect(20, 20, 300, Screen.height - 20), myLog, textStyle);
    }
    //#endif
}