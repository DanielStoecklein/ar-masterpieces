﻿
using System;

namespace UnsplashSearch
{
    [Serializable]
    public class SearchResults
    {
        public int total;
        public int total_pages;
        public Photo[] results;

    }
}

