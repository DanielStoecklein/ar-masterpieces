﻿

namespace UnsplashSearch
{
    public class PhotoLinks
    {
        public string self;
        public string html;
        public string download;
    }
}