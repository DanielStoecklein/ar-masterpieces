﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class PreviewImageScript : MonoBehaviour
{
    public SingleUnsplashResult result;
    public GameObject previewImmage;
    private Texture shownImage;
    public GameObject Failure;
    public GameObject PreviewView;


    // Update is called once per frame
    void Update()
    {
        if (shownImage != previewImmage.GetComponent<RawImage>().texture)
        {
            previewImmage.GetComponent<RawImage>().texture = result.FullImmage;
            shownImage = result.FullImmage;
        }

    }

    public IEnumerator LoadPicture()
    {
        UnityWebRequest request = UnityWebRequestTexture.GetTexture(result.photo.urls.regular);
        Debug.Log("Downloading from: " + result.photo.urls.regular);
        yield return request.SendWebRequest();
        if (request.isNetworkError || request.isHttpError)
        {
            Debug.Log(request.error);
            ShowFailureMessage();
        }

        else
        {
            previewImmage.GetComponent<RawImage>().texture = ((DownloadHandlerTexture)request.downloadHandler).texture;
            AdjustImageSize(((DownloadHandlerTexture)request.downloadHandler).texture);
            result.FullImmage = previewImmage.GetComponent<RawImage>().texture;
            transform.GetChild(0).gameObject.GetComponent<Text>().text = result.GetAttributons();
        }

    }

    private void ShowFailureMessage()
    {
        PreviewView.SetActive(false);
        Failure.SetActive(true);
    }



    private const float StandardImageSize = 420f;
    private const float MaxWidth = 1760f;
    private const float MaxHeight = 460f;


    private void AdjustImageSize(Texture2D image)
    {
        RawImage preview = previewImmage.GetComponent<RawImage>();
        float width = image.width;
        //Debug.Log(image.width + " = orig width");

        float height = image.height;
        //Debug.Log(image.height + " = orig height");

        float heightWidthRatio = (height / width);
        //Debug.Log(heightWidthRatio + " = h/w ratio");
        if (heightWidthRatio > 1f)
        {
            //Debug.Log(heightWidthRatio + " 1");
            preview.rectTransform.sizeDelta = new Vector2(StandardImageSize, StandardImageSize * heightWidthRatio);
        }
        else
        {
            //Debug.Log(heightWidthRatio + " 2");
            preview.rectTransform.sizeDelta = new Vector2(StandardImageSize / heightWidthRatio, StandardImageSize);
        }

        //Skalliere die Bilder so, dass sie die Maximalausmaße nicht überschreiten
        //->zu hoher wert wird auf max wert gestellt 
        //->anderer Wert angepasst um relationen nicht zu verändern
        if (preview.rectTransform.sizeDelta.x > MaxWidth)
        {

            float overwidth = preview.rectTransform.sizeDelta.x / MaxWidth;

            float adjustedHeight = preview.rectTransform.sizeDelta.y / overwidth;
            preview.rectTransform.sizeDelta = new Vector2(MaxWidth, adjustedHeight);
        }

        if (preview.rectTransform.sizeDelta.y > MaxHeight)
        {
            float overheight = preview.rectTransform.sizeDelta.y / MaxHeight;
            float adjustedWidth = preview.rectTransform.sizeDelta.x / overheight;
            preview.rectTransform.sizeDelta = new Vector2(adjustedWidth, MaxHeight);
        }
    }
}
