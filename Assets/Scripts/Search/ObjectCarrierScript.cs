﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.PlayerLoop;

public class ObjectCarrierScript : MonoBehaviour
{

    public bool loaded = false;
    private void Update()
    {
        GameObject[] objs = GameObject.FindGameObjectsWithTag("DontDestroy");
        if (objs.Length > 1 && loaded)
        {
            Debug.Log("Destroying Carrier");
            Destroy(this.gameObject);
        }

    }
    public void setLoad()
    {
        loaded = true;
    }

}
