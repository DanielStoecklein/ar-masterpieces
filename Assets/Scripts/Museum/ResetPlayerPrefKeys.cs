﻿//Author: Haris Hodzic

using UnityEngine;
using UnityEngine.SceneManagement;

public class ResetPlayerPrefKeys : MonoBehaviour
{
    public void resetKeys()
    {

        PlayerPrefs.DeleteAll();
        PlayerPrefs.Save();
        PlayerPrefs.SetString("Tutorial", "Finished");
        SceneManager.LoadScene("Museum");
        
        
    }
}
