﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneLoader : MonoBehaviour
{

    //Daniel Stöcklein
    private string activeScene;
    [SerializeField]
    private bool experimentalARMerge = false;
    public bool IsExperimentalModeActive()
    {
        return experimentalARMerge;
    }
    public void SetExperimentalARMerge(bool active)
    {
        experimentalARMerge = active;
    }

    private void Start()
    {
        SceneManager.sceneLoaded += Action_SceneLoaded;
        DontDestroyOnLoad(this);
    }



    public void LoadScene(string scenename)
    {

        if (!SceneManager.GetSceneByName(scenename).isLoaded)
        {

            StartCoroutine(LoadSceneAdditive(scenename));
        }
        else
        {
            ActivateYourScene(scenename);
        }
    }


    public IEnumerator LoadSceneAdditive(string scenename)
    {

        SceneManager.LoadSceneAsync(scenename, LoadSceneMode.Additive);
        yield return new WaitForEndOfFrame();

    }

    public void Action_SceneLoaded(Scene LoadedScene, LoadSceneMode mode)
    {
        ActivateYourScene(LoadedScene.name);
    }

    public void SetRootObjects(bool active, string nextScene, string lastScene)
    {
        GameObject[] arr = SceneManager.GetActiveScene().GetRootGameObjects();
        if (experimentalARMerge && (lastScene == "Augmenting Masterpieces" && nextScene == "ARSculpture")
                                    || (lastScene == "ARSculpture" && nextScene == "Augmenting Masterpieces"))
        {
            foreach (GameObject a in arr)
                if (a.name == "Holder" || ((a.name.Contains("Marker Holder") || a.name == "HitMarker(Clone)") && active))
                    a.SetActive(active);
            Debug.Log("CanvasMoveDevis.SetActive = " + active);
            Debug.Log("lastscene = " + lastScene + " and nextscene = " + nextScene);
        }
        else
        {
            foreach (GameObject a in arr)
                if (a.name == "Holder" || a.name.Contains("Marker Holder") || a.name == "HitMarker(Clone)" || (a.name == "CanvasMoveDevice" && active == false))
                    a.SetActive(active);
            Debug.Log("CanvasMoveDevis.SetActive = " + active);
            Debug.Log("lastscene = " + lastScene + " and nextscene = " + nextScene);

        }
    }

    public void ActivateYourScene(string sceneName)
    {
        string lastScene = SceneManager.GetActiveScene().name; //Daniel Stöcklein, nur in Variable gespeichert, damit man nicht immer GetActiveScene aufrufen muss
        SetRootObjects(false, sceneName, lastScene);
        SceneManager.SetActiveScene(SceneManager.GetSceneByName(sceneName));
        activeScene = SceneManager.GetActiveScene().name; //Daniel Stöcklein, nur in Variable gespeichert, damit man nicht immer GetActiveScene aufrufen muss
        Debug.Log(activeScene);
        SetRootObjects(true, sceneName, lastScene);

        //Daniel Stöcklein
        if (activeScene == "Augmenting Masterpieces") SetARState("pic");
        if (activeScene == "ARSculpture") SetARState("poly");
    }

    //Daniel Stöcklein
    private void SetARState(string picORpoly)
    {
        GameObject sideMenu = null;
        if (picORpoly == "pic")
        {
            sideMenu = GameObject.FindGameObjectWithTag("SideMenu");
        }
        else if (picORpoly == "poly")
        {
            sideMenu = GameObject.FindGameObjectWithTag("SideMenu2");
        }
        else
        {
            Debug.Log("State not known");
            return;
        }
        sideMenu.GetComponent<SimpleSideMenu>().Close();
    }
}
