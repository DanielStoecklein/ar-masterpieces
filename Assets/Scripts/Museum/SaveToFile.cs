﻿// Author: Haris Hodzic

using UnityEngine;
using System.IO;
using System;
using UnityEngine.SceneManagement;

[System.Serializable]

public class SaveToFile : MonoBehaviour
{
    public static bool sceneCheck;
    public GameObject panel2;

    public void SaveToTxtData() {
        
        panel2.SetActive(false);
        Texture2D picture = PersistentManager.Instance.picture.mainTexture as Texture2D;

        string fileName = System.DateTime.Now.ToString("dd-MM-yyyy-HH-mm-ss") + ".jpg";

        string filePath = Application.persistentDataPath + "/Raum/" + fileName;

        File.WriteAllBytes(filePath, picture.EncodeToJPG()); //speichern des Bildes im Applicationsordner

        string pathtoTxt = Application.persistentDataPath + "/" + "MaterialForMuseum.txt";

        File.AppendAllText(pathtoTxt, filePath + Environment.NewLine);

        SceneManager.LoadScene("Museum");
        sceneCheck = true;
    }
}