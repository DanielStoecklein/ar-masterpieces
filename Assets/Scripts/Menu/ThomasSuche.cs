﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class ThomasSuche : MonoBehaviour
{
	public void StartSearchScene() => SceneManager.LoadScene("SearchMenu");
}
