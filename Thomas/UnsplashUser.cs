﻿using System;

namespace UnsplashSearch
{
    [Serializable]
    public class UnsplashUser
    {
        public string username;
        public string name;
        public string instagramm_username;
        public string twitter_username;
        public string portfolio_url;
        public ProfileImage ProfileImage;
        public UserLinks Links;
    }
}
