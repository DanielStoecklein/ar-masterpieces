﻿//Author: Haris Hodzic

using UnityEngine;

public class RayCastPicture : MonoBehaviour
{
    public GameObject panel;
    
    public void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

            if (Physics.Raycast(ray,out RaycastHit hit, 6))
            {
                if (hit.transform.name == "Paint" || hit.transform.name == "Holder" || hit.transform.name == "Glass") 
                {
                    if (panel.gameObject.activeSelf == false)
                    {
                        panel.gameObject.SetActive(true);
                        
                    } else
                    {
                        panel.gameObject.SetActive(false);
                    }
                }
            }
        }
        staticRaycast.Update();
    }
    
}