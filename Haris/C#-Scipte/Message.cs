﻿// Autor: Haris Hodzic


using UnityEngine;
using UnityEngine.UI;

public class Message : MonoBehaviour
{
    public static void messageTxt(string msg)
    {
        //Prefab MessageBox mit Bestandteilen wird aus Resource-Ordner geladen
        GameObject messagePrefab = Resources.Load("MessageBox") as GameObject;
        GameObject containerObject = messagePrefab.gameObject.transform.GetChild(0).gameObject;
        GameObject textObject = containerObject.gameObject.transform.GetChild(0).GetChild(0).gameObject;

        //Stringvariable wird TextObjekt zugewiesen
        Text msg_text = textObject.GetComponent<Text>();
        msg_text.text = msg;

        //Objektkopie wird erstellt (Position und Rotation können bei dem Klon angepasst werden)
        GameObject clone = Instantiate(messagePrefab);

        removeMsg(clone); //löschen der Kopie
    }

    public static void removeMsg(GameObject clone)
    {
        Destroy(clone.gameObject, 3f);
    }
}
