﻿// Autor: Haris Hodzic

using NUnit.Framework.Constraints;
using System;
using System.Collections;
using System.IO;
using UnityEngine;
using UnityEngine.UI;

public class Screenshot : MonoBehaviour
{
    //VariablenDeklaration
    public Button shareButton;
    private bool isFocus = false;
    private bool isProcessing = false;

    void OnApplicationFocus(bool focus)
    {
        isFocus = focus;
    }

    void Start()
    {
        shareButton.onClick.AddListener(OnShareButtonClick);
    }

    public void OnShareButtonClick()
    {
        if (!isProcessing) // erst nachdem TakeScreenshotAndSave() - Methode abgeschlossen ist, kann sie erneut durchgeführt werden
        {
            StartCoroutine(TakeScreenshotAndSave()); //Coroutine kann die Ausführung pausieren und später wieder aufnehmen
        }
    }

    private IEnumerator TakeScreenshotAndSave()
    {
        isProcessing = true;

        // Dateiname, bestehend aus zeitstempel und Dateiendung
        string fileName = System.DateTime.Now.ToString("dd-MM-yyyy-HH-mm-ss") + ".png";

        //UI verbergen
        yield return null; //Ausführung wird paussiert und im nächsten frame wieder aufgenommen
        GameObject.Find("Canvas").GetComponent<Canvas>().enabled = false;

        //Screen wird gerendert
        yield return new WaitForEndOfFrame(); //

        //2D Textur vom Bildschirm wird erstellt
        Texture2D screenImage = new Texture2D(Screen.width, Screen.height);
        screenImage.ReadPixels(new Rect(0, 0, Screen.width, Screen.height), 0, 0);
        screenImage.Apply();

        //Konvertierung in PNG und speichern in Byte Array
        byte[] imageBytes = screenImage.EncodeToPNG();

        String filePath = Application.persistentDataPath + "/" + fileName;
        File.WriteAllBytes(filePath, screenImage.EncodeToPNG()); //speichern des Bildes im Applicationsordner
        
        Debug.Log(filePath);

        NativeGallery.Permission perm = NativeGallery.CheckPermission();

        if (perm == NativeGallery.Permission.Denied)
        {
            NativeGallery.OpenSettings(); //Einstellungen öffnen um Berechtigungen anzupassen
        }

        if (perm == NativeGallery.Permission.Granted)
        {
            //Speichern in Galerie unter "Camera"
            NativeGallery.SaveImageToGallery(imageBytes, "Camera", fileName, null);
        }
        
        //weitere Prüfung hier noch Notwendig
        if (File.Exists(filePath)) 
        {
            Message.messageTxt("Bild gespeichert!");
        }
        else
        {
            Message.messageTxt("Fehler aufgetreten!");
        }


#if !UNITY_EDITOR && UNITY_ANDROID
        
        if (!Application.isEditor)
        {
            AndroidJavaClass unity = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
            AndroidJavaObject currentActivity = unity.GetStatic<AndroidJavaObject>("currentActivity");
           
            AndroidJavaClass intentClass = new AndroidJavaClass("android.content.Intent");
            AndroidJavaObject intentObject = new AndroidJavaObject("android.content.Intent");
            intentObject.Call<AndroidJavaObject>("setAction", intentClass.GetStatic<string>("ACTION_SEND"));

            //Dateiobjekt vom Screenshot erstellen
            AndroidJavaObject fileObject = new AndroidJavaObject("java.io.File", filePath);

            //FileProviderklasse wird erstellt (androidx.core.content.FileProvider verwenden, bei  
            AndroidJavaClass fileProviderClass = new AndroidJavaClass("androidx.core.content.FileProvider");

            //Array vom Typ Object wird erstellt und es wird 
            //currentActivity, PackageName(Project Settings->Player->Other Settings->Identification), Bildpfad ihm zugewiesen
            object[] providerParams = new object[3];
            providerParams[0] = currentActivity;
            providerParams[1] = "com.ARTeam.AugmentingMasterpieces.provider";
            providerParams[2] = fileObject;

            //Objekt wird erstellt mittels getUriForFile()-Methode, dem Parameter übergeben werden
            AndroidJavaObject uriObject = fileProviderClass.CallStatic<AndroidJavaObject>("getUriForFile", providerParams);

            intentObject.Call<AndroidJavaObject>("putExtra", intentClass.GetStatic<string>("EXTRA_STREAM"), uriObject);
            //Typ wird als PNG-Bild festgelegt
            intentObject.Call<AndroidJavaObject>("setType", "image/png"); 

            //Berechtigung wird gegeben um URI zu lesen
            intentObject.Call<AndroidJavaObject>("addFlags", intentClass.GetStatic<int>("FLAG_GRANT_READ_URI_PERMISSION"));

            AndroidJavaObject chooser = intentClass.CallStatic<AndroidJavaObject>("createChooser", intentObject, "....");
            currentActivity.Call("startActivity", chooser);
            
            // UI wieder anzeigen
            GameObject.Find("Canvas").GetComponent<Canvas>().enabled = true;
        }

        yield return new WaitUntil(() => isFocus);
        isProcessing = false;
    }
#endif
    }
}




