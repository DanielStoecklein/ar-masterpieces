﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PermissionsIOS : MonoBehaviour
{
    public static void AskForCameraPermission()
    {
        if (!Application.HasUserAuthorization(UserAuthorization.WebCam))
        {
            Application.RequestUserAuthorization(UserAuthorization.WebCam);
        }
    }
    public static void AskForMicrophonePermission()
    {
        if (!Application.HasUserAuthorization(UserAuthorization.Microphone))
        {
            Application.RequestUserAuthorization(UserAuthorization.Microphone);
        }
    }
}

