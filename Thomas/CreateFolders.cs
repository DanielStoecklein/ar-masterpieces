﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.Android;

public class CreateFolders : MonoBehaviour
{
    /* 
    * Creates folders that are located in the directory directectly associated with the app if not already in place.
    * Android: storage/emulated/0/Android/data/[name].[of].[app-package]/files/[folder-name]
    */
    //dada
    public static void CreateInternalFolders(string[] folderNames)
    {
        foreach (string fn in folderNames)
        {
            string path = Path.Combine(Application.persistentDataPath, fn);
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
        }
    }

    /* 
     * Creates folders that are located in directories open to all apps with the necessary rigths to wright and read data if not already in place.
     * Android: storage/emulated/0/[app-name]/[folder-name]
     */

    public static void CreateExternalFolders(string[] folderNames)
    {
        if (Application.platform == RuntimePlatform.Android)
        {
            foreach (string fn in folderNames)
            {
                string path = Path.Combine(Path.Combine(GetAndroidExternalStoragePath(), Application.productName), fn);

                if (!Directory.Exists(path))
                {
                    Directory.CreateDirectory(path);
                }
            }
        }
    }


    /*
     * Finds the path where Android stores its external data
     * returns a path as a string 
     */
    public static string GetAndroidExternalStoragePath()
    {
        string path = "";
        try
        {
            AndroidJavaClass jc = new AndroidJavaClass("android.os.Environment");
            path = jc.CallStatic<AndroidJavaObject>("getExternalStorageDirectory").Call<string>("getAbsolutePath");

        }
        catch (Exception e)
        {
            Debug.Log(e.Message);
        }
        return path;
    }

}

