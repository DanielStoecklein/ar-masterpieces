﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems;

[RequireComponent(typeof(ObjectSpawner))]
public class PlacementIndicator : MonoBehaviour
{
    private ARRaycastManager arRaycastManager;
    private GameObject indicator;
    private GameObject verticalPlane;
    private Vector2 screenMiddle = new Vector2(Screen.width / 2, Screen.height / 2);
    private List<ARRaycastHit> hits = new List<ARRaycastHit>();
    private bool verticalPlaneActivated = false;

    public bool IsVerticalPlaneActivated() {
        return this.verticalPlaneActivated;
    }
    public GameObject GetVerticalPlane() {
        return this.verticalPlane;
    }

    ObjectSpawner spawnerScript;

    void ShowGameObjectIfNotYetActive(GameObject obj)
    {
        if (!obj.activeInHierarchy)
        {
            obj.SetActive(true);
            Debug.Log(obj.name + " activated!");
        }
    }

    //if Raycast hits a Plane, update the position and rotation
    void UpdatePoseWhenRaycastHitsPlane(GameObject obj)
    {
        if (arRaycastManager.Raycast(screenMiddle, hits, TrackableType.PlaneWithinPolygon))
        {
            obj.transform.position = hits[0].pose.position;
            obj.transform.rotation = hits[0].pose.rotation;
        }

    }

    void Start()
    {
        //get the components
        arRaycastManager = FindObjectOfType<ARRaycastManager>();
        indicator = transform.GetChild(0).gameObject;
        verticalPlane = transform.GetChild(1).gameObject;

        //hide the placement indicator at Start
        indicator.SetActive(false);
        verticalPlane.SetActive(false);

        spawnerScript = GetComponent<ObjectSpawner>();
        spawnerScript.enabled = false;
    }

    void Update()
    {

        if (verticalPlaneActivated == false)
        {

            UpdatePoseWhenRaycastHitsPlane(transform.gameObject);
            
            
            if(hits.Count > 0) //activate indicator when there was the first hit
            {
                ShowGameObjectIfNotYetActive(indicator);
            }
            //If User Touches Screen Vertical Plane gets activated and positioned on the Position of the hit/Marker
            if (Input.touchCount >= 1 && Input.GetTouch(0).phase == TouchPhase.Began)
            {
                ShowGameObjectIfNotYetActive(verticalPlane);
                verticalPlaneActivated = true;
            }
        }
        else //Vertical Plane is activated and Marker locked => Picture Object can now be placed
        {
            spawnerScript.enabled = true;
        }
    }
}
