﻿
using System;

namespace UnsplashSearch
{
    [Serializable]
    public class Photo
    {
        public string id;
        public string created_at;
        public int width;
        public int height;
        public string color;
        public string description;
        public string alt_description;
        public UnsplashUser user;
        public PhotoUrls urls;
        public PhotoLinks links;
    }

}

