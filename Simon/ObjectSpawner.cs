﻿using System.Collections.Generic;
using UnityEngine;


public class ObjectSpawner : MonoBehaviour
{
    private bool instantiated = false;
    public GameObject objectToSpawn;
    private GameObject placedObject;
    // private List<GameObject> placedObjects = new List<GameObject>();
    private GameObject verticalPlane;
    private Camera arCamera;
    RaycastHit hitObject;
    Ray ray;

    void Start()
    {
        arCamera = Camera.main;
        verticalPlane = transform.GetChild(1).gameObject;
        Debug.Log("spawnerScript enabled");
    }
    void Update()
    {
        if(instantiated == false)
        {
            if (Input.touchCount >= 1 && Input.GetTouch(0).phase == TouchPhase.Began) 
            {  
                ray = arCamera.ScreenPointToRay(Input.GetTouch(0).position);
                if (Physics.Raycast(ray, out hitObject))
                {
                    if(hitObject.transform.gameObject.name == verticalPlane.name)
                    {
                        placedObject = Instantiate(objectToSpawn, hitObject.point,verticalPlane.transform.rotation);
                        placedObject.transform.Rotate(0.0f, 0.0f, 180.0f, Space.Self);
                        Debug.Log(placedObject.name + " instantiated");
                        // placedObjects.Add(placedObject);
                        instantiated = true;
                    }
                }
                else 
                {
                    Debug.Log("Didn't touch vertical Plane: hitObject: " + hitObject.transform.gameObject.name + "!=" + "verticalPlane: " + verticalPlane.name);
                }
            }
        }
    }
}
