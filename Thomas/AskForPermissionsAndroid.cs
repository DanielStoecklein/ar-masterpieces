﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Android;

/*
 * Collection of methods that can be called to ask for an permission to use a certain apect of the Android.Phone
*/

public class AskForPermissionsAndroid : MonoBehaviour
{
    /*
     * Asks for needed ungranted permissions on startup
     */
    void Start()
    {
        StartCoroutine(AskForNecessaryPermissions());


        string[] inFolNam = { "Screenshots", "3dObjekte", "Bilder", "Rahmen", "Podeste", "Raum", "Sonstige Dateien" };
        string[] exFolNam = { "Screenshots" };
        CreateFolders.CreateInternalFolders(inFolNam);
        CreateFolders.CreateExternalFolders(exFolNam);

    }

    private IEnumerator AskForNecessaryPermissions()
    {
        List<bool> permissions = new List<bool>() { false, false };
        List<bool> permissionsAsked = new List<bool>() { false, false };
        List<Action> actions = new List<Action>()
    {
        new Action(() => {
            permissions[0] = Permission.HasUserAuthorizedPermission(Permission.ExternalStorageWrite);
            if (!permissions[0] && !permissionsAsked[0])
            {
                Permission.RequestUserPermission(Permission.ExternalStorageWrite);
                permissionsAsked[0] = true;
                permissions[0] = Permission.HasUserAuthorizedPermission(Permission.ExternalStorageWrite);
                return;
            }
        }),
        new Action(() => {
            permissions[1] = Permission.HasUserAuthorizedPermission(Permission.Camera);
            if (!permissions[1] && !permissionsAsked[1])
            {
                Permission.RequestUserPermission(Permission.Camera);
                permissionsAsked[1] = true;
                permissions[1] = Permission.HasUserAuthorizedPermission(Permission.ExternalStorageWrite);
                return;
            }
        })

    };
        for (int i = 0; i < permissionsAsked.Count;)
        {
            actions[i].Invoke();
            if (permissions[i])
            {
                ++i;
            }
            yield return new WaitForEndOfFrame();
        }
    }


    public static void AskForCameraPermission()
    {
        if (!Permission.HasUserAuthorizedPermission(Permission.Camera))
        {
            Permission.RequestUserPermission(Permission.Camera);
        }
    }

    public static void AskForExternalReadPermission()
    {
        if (!Permission.HasUserAuthorizedPermission(Permission.ExternalStorageRead))
        {
            Permission.RequestUserPermission(Permission.ExternalStorageRead);
        }
    }

    public static void AskForExternalWritePermission()
    {
        if (!Permission.HasUserAuthorizedPermission(Permission.ExternalStorageWrite))
        {
            Permission.RequestUserPermission(Permission.ExternalStorageWrite);
        }
    }

    public static void AskForCoarsePositionPermission()
    {
        if (!Permission.HasUserAuthorizedPermission(Permission.CoarseLocation))
        {
            Permission.RequestUserPermission(Permission.CoarseLocation);
        }
    }

    public static void AskForPrecisePositionPermission()
    {
        if (!Permission.HasUserAuthorizedPermission(Permission.FineLocation))
        {
            Permission.RequestUserPermission(Permission.FineLocation);
        }
    }

    public static void AskForMicrophonePermission()
    {
        if (!Permission.HasUserAuthorizedPermission(Permission.Microphone))
        {
            Permission.RequestUserPermission(Permission.Microphone);
        }
    }

}
