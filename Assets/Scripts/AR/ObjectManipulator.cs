using UnityEngine;
using UnityEngine.UI;
//Author: Daniel Stöcklein

/// <summary>This class performs basic manipulation for augmented-objects</summary>
public class ObjectManipulator : MonoBehaviour
{
    private float initialFingersDistance;
    private Vector3 initialScale;
    private float minScale = 0.1f;
    private float maxScale = 0.9f;
    private GameObject curSelected;
    private Transform verticalPlane;
    private Touch touch;
    private Touch[] touches;
    private UIController uiController;
    private bool uiTouched;
    private Selectable selectable;
    private ObjectSpawner objectSpawner;
    [SerializeField]
    private GameObject newPictureButton;
    [SerializeField]
    private GameObject destroyPictureButton;


    [SerializeField]
    public Camera cam;

    void Start()
    {
        uiController = new UIController();
        selectable = new Selectable();
        objectSpawner = GetComponent<ObjectSpawner>(); //Simon Weickert
    }

    void Update()
    {
        if (Input.touchCount > 0)
        {
            touch = Input.touches[0];
            touches = Input.touches;
            uiTouched = uiController.IsTouchOverUIObject(touch.position); //touched on UI-Element?

            switch (touch.phase)
            {
                case TouchPhase.Began: //First touch detected, perform Raycast
                    DoRaycast();
                    break;
                case TouchPhase.Moved: //Touch movement detected, perform dragging 
                    if (curSelected != null && Input.touchCount == 1)
                    {
                        DragSelected();
                    }
                    break;
            }
            if (curSelected != null && Input.touchCount == 2) //Performs Scaling
            {
                ScaleSelected();
            }
        }
    }

    /// <summary>Performs Raycast and checks if a picture got hit</summary>
    void DoRaycast()
    {
        Ray ray = cam.ScreenPointToRay(touch.position);
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit))
        {
            //Check if something got hit and if its a frame
            if (hit.collider.gameObject != null && (hit.transform.name.Contains("frame1") || hit.transform.name.Contains("frame2")) || hit.transform.name.Contains("frame3"))
            {
                if (!uiTouched && curSelected != null)
                {
                    selectable.Deselect(curSelected);
                    newPictureButton.SetActive(true);
                    destroyPictureButton.SetActive(false);
                    curSelected = null;
                }
                if (!uiTouched)
                {
                    curSelected = hit.collider.gameObject;
                    selectable.Select(curSelected);
                    newPictureButton.SetActive(false);
                    destroyPictureButton.SetActive(true);
                }
                verticalPlane = curSelected.transform.parent;
            }
            else
            {
                if (!uiTouched && curSelected != null)
                {
                    selectable.Deselect(curSelected);
                    newPictureButton.SetActive(true);
                    destroyPictureButton.SetActive(false);
                    curSelected = null;
                }
            }
        }
    }

    /// <summary>Performs pinch scale </summary>
    void ScaleSelected()
    {
        Touch t1 = touches[0];
        Touch t2 = touches[1];

        if (t1.phase == TouchPhase.Began || t2.phase == TouchPhase.Began)
        {
            initialFingersDistance = Vector2.Distance(t1.position, t2.position);
            initialScale = curSelected.transform.localScale; //get initial scale
        }
        else if (t1.phase == TouchPhase.Moved || t2.phase == TouchPhase.Moved)
        {
            float currentFingersDistance = Vector2.Distance(t1.position, t2.position);
            float scaleFactor = currentFingersDistance / initialFingersDistance;

            Vector3 newScale = initialScale * scaleFactor;
            if (newScale.x > minScale && newScale.y > minScale && newScale.x < maxScale && newScale.y < maxScale)
            {
                curSelected.transform.localScale = newScale;
            }
        }

        objectSpawner.setMeasureTextOfPlacedObject(curSelected); //Simon Weickert

    }

    /// <summary>Performs dragging</summary>
    void DragSelected()
    {
        if (verticalPlane.name.Contains("VerticalPlane"))
        {
            //convert from pixel to viewportpoints, to get app running on all resolutions
            Vector3 curPos = cam.ScreenToViewportPoint(touch.position);
            Vector3 lastPos = cam.ScreenToViewportPoint(touch.position - touch.deltaPosition);
            Vector3 touchDir = curPos - lastPos;
            touchDir.z = 0;

            //Drag along vertical plane
            curSelected.transform.position += verticalPlane.transform.TransformDirection(touchDir);
        }
        else
        {
            //convert from pixel to viewportpoints, to get app running on all resolutions
            Vector3 curPos = cam.ScreenToViewportPoint(touch.position);
            Vector3 lastPos = cam.ScreenToViewportPoint(touch.position - touch.deltaPosition);
            Vector3 touchDir = curPos - lastPos;
            Vector3 planeDir;
            planeDir.x = touchDir.x;
            planeDir.y = 0;
            planeDir.z = touchDir.y;

            //Drag along vertical plane
            curSelected.transform.position += verticalPlane.transform.TransformDirection(planeDir);
        }
    }

    public void DestroyPicture()
    {
        Destroy(curSelected);
        newPictureButton.SetActive(true);
        destroyPictureButton.SetActive(false);
    }
}