﻿//Author: Haris Hodzic

using Boo.Lang;
using UnityEngine;
using UnityEngine.UI;
using System;

public class showPicture : MonoBehaviour
{
    public Button btn;
    GameObject paint;
    
    public void changePicture()
    {
        string name = staticRaycast.Update();
        paint = GameObject.Find(name).transform.Find("Holder").transform.Find("Paint").gameObject;
        
        Texture2D texture = btn.GetComponent<Image>().sprite.texture as Texture2D;
        byte[] texAsByte = texture.EncodeToPNG();
        string texAsString = Convert.ToBase64String(texAsByte);
        string myKey = name;
        PlayerPrefs.SetString(myKey, texAsString);
        PlayerPrefs.Save();

        paint.GetComponent<Renderer>().material.SetTexture("_MainTex", texture);
        //panel.transform.parent.transform.parent.transform.parent.gameObject.SetActive(false);
    }
}
