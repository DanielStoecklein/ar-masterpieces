﻿using System;

namespace UnsplashSearch
{
    [Serializable]
    public class UserLinks
    {
        public string self;
        public string html;
        public string photos;
        public string likes;
    }
}