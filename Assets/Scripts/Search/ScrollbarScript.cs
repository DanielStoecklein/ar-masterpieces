﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnsplashSearch;

public class ScrollbarScript : MonoBehaviour
{
    public UnsplashRequestHandler urh;
    public PolyRequestHandler prh;
    float TimeOfLastLoad;


    public void MoreUnsplashResults()
    {
        if ((Time.time - TimeOfLastLoad) < 0.5 || (urh.loading))
            return;
        Scrollbar sb = this.GetComponent<Scrollbar>();
        if (sb.value < 0.01f)
            urh.LoadNextPage();
        TimeOfLastLoad = Time.time;

    }
    public void MorePolyResults()
    {
        if ((Time.time - TimeOfLastLoad) < 0.5 || (prh.loading))
            return;
        Scrollbar sb = this.GetComponent<Scrollbar>();
        if (sb.value < 0.01f)
            prh.LoadNextPage();
        TimeOfLastLoad = Time.time;
    }
}
