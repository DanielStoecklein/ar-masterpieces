﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Author: Daniel Stöcklein
public static class TagSearcher
{
    /// <summary>
    /// Finds childs with a specific tag and returns it
    /// </summary>
    public static GameObject FindObjectsWithTag(this Transform parent, string tag)
    {
        for (int i = 0; i < parent.childCount; i++)
        {
            Transform child = parent.GetChild(i);
            if (child.CompareTag(tag))
            {
                return child.gameObject;
            }
        }
        return null;
    }
}
