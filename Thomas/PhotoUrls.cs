﻿namespace UnsplashSearch
{
    public class PhotoUrls
    {
        public string raw;
        public string full;
        public string regular;
        public string small;
        public string thumb;
    }
}