﻿using System;

namespace UnsplashSearch
{
    [Serializable]
    public class ProfileImage
    {
        public string test;
        public string small;
        public string medium;
        public string large;
    }
}